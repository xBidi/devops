#!/usr/bin/env bash
git checkout --orphan temp_branch
git add -A
git commit -am "initial commit"
git branch -D master
git branch -m master
git push -f origin master
git branch --set-upstream-to=origin/master master