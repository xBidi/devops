#!/usr/bin/env bash
docker image build -t apache2 .
docker stop apache2
docker rm apache2
cp public-html/* /Users/diegotobalina/develop/docker/apache2/htdocs
docker run -p 8080:80 --volume /Users/diegotobalina/develop/docker/apache2/htdocs:/usr/local/apache2/htdocs/ --name apache2 -d apache2
