#!/usr/bin/env bash
sudo docker volume rm gitlab_etc
sudo docker volume rm gitlab_log
sudo docker volume rm gitlab_opt
sudo docker volume create gitlab_etc
sudo docker volume create gitlab_log
sudo docker volume create gitlab_opt
sudo docker stop gitlab
sudo docker rm gitlab
sudo docker run --detach \
  --hostname localhost \
  --publish 9000:80\
  --name gitlab \
  --restart always \
  --volume gitlab_etc:/etc/gitlab \
  --volume gitlab_log:/var/log/gitlab \
  --volume gitlab_opt:/var/opt/gitlab \
  gitlab/gitlab-ce:latest
