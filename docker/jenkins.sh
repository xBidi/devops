docker volume rm jenkins
docker volume create jenkins
docker stop jenkins
docker rm jenkins
docker run -p 8080:8080 -p 50000:50000 --name jenkins --volume jenkins:/jenkins_config -d jenkins/jenkins
docker exec -it jenkins bash
cat /var/jenkins_home/secrets/initialAdminPassword
