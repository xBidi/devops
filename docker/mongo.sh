docker volume rm mongo
docker volume create mongo
docker stop mongo
docker rm mongo
docker run -p 27017:27017 --volume mongo:/data/db --name mongo -d mongo
docker exec -it mongo bash