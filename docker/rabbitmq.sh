#!/usr/bin/env bash
docker stop rabbitmq
docker rm rabbitmq
docker run -d -p 15672:15672 -p 5672:5672 --hostname rabbitmq --name rabbitmq -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password rabbitmq:3-management
